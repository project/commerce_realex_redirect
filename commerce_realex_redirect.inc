<?php

/**
 * @file
 * Implements Realex Payment Method in Drupal Commerce checkout.
 */


/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_realex_redirect_commerce_payment_method_info() {
  $payment_methods = array();

  $icons = commerce_realex_redirect_icons();
  $display_title = t('!logo Realex - The global realtime payments exchange', array('!logo' => $icons['realex']));
  $display_title .= '<div class="commerce-realex-icons"><span class="label">' . t('Includes:') . '</span>' . implode(' ', $icons) . '</div>';

  $payment_methods['realex_redirect'] = array(
    'base' => 'commerce_realex_redirect',
    'title' => t('Realex Redirect Payments'),
    'short_title' => t('Realex Redirect'),
    'display_title' => $display_title,
    'description' => t('Realex global realtime payments exchange'),
    'terminal' => FALSE,
    'offsite' => TRUE,
    'offsite_autoredirect' => TRUE,
  );

  return $payment_methods;
}

/**
 * Payment method callback: settings form.
 */
function commerce_realex_redirect_settings_form($settings = NULL) {
  global $base_url;
  $form = array();
  // Merge default settings into the stored settings array.
  $default_currency = variable_get('commerce_default_currency', 'USD');
  $form['explanation'] = array(
	'#markup' => '<ul>
	<li>The Realex gateway requires the url to the Instant Payment notification, which is as follows [your website domain]/commerce_realex/ipn.  This has to be entered into the admin area of http://www.realexpayments.com/.</li>
	<li>The url above is used for <b>both</b> i) sending the ipn to your website and ii) providing the content for the response page.</li>
	<li>The response page is the page that is used after a payment is put through.  The user is not directed there, rather the content of that page is scraped and displayed by Realex.  The commerce_realex module uses the template commerce-realex-response-page.tpl.php for this purpose and a javascript redirect to the url set below under Response Page.  This template can be overridden by the usual method of creating a namesake in your theme directory.</li>
	<li>The Realex gateway also allows for an html template to be used to format both the Response Page and the Payment page.  The Payment Page is the page that is presented allowing the purchaser to securely enter their credit card details.</li> 
	</ul>'
  );
  $settings = (array) $settings + array(
    'merchant_id' => '',
    'currency_code' => in_array($default_currency, array_keys(commerce_realex_redirect_currencies())) ? $default_currency : 'USD',
	'server' => 'https://epage.payandshop.com/epage.cgi',
	'response_page' => $base_url. '/checkout/complete',
    'language' => 'US',
    'payment_action' => 'sale',
    'ipn_logging' => 'notification',
  );

  $form['merchant_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Realex member ID'),
    '#description' => t('The member ID for the Realex account where you want to receive payments.'),
    '#default_value' => $settings['merchant_id'],
    '#required' => TRUE,
  );
  
  $form['shared_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Shared Secret'),
    '#description' => t('The shared secret for the Realex account.'),
    '#default_value' => $settings['shared_secret'],
    '#required' => TRUE,
  );
  
  $form['server'] = array(
    '#type' => 'textfield',
    '#title' => t('Server URL'),
    '#description' => t('The url to the Realex server where payments are processed.'),
    '#default_value' => $settings['server'],
    '#required' => TRUE,
  );
  
  $form['response_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Response Page'),
    '#description' => t('This is the page which the default commerce-realex-response-page template redirects to. Ensure that this is a complete url with your domain name.'),
    '#default_value' => $settings['response_page'],
    '#required' => TRUE,
  );
  
  $form['currency_code'] = array(
    '#type' => 'select',
    '#title' => t('Currency code'),
    '#description' => t('Transactions can only be processed in one of the listed currencies.'),
    '#options' => commerce_realex_redirect_currencies(),
    '#default_value' => $settings['currency_code'],
  );
  
  $form['language'] = array(
    '#type' => 'select',
    '#title' => t('Realex login page language'),
    '#options' => commerce_realex_redirect_languages(),
    '#default_value' => $settings['language'],
  );
  
  $form['payment_action'] = array(
    '#type' => 'radios',
    '#title' => t('Payment action'),
    '#options' => array(
      'sale' => t('Sale - authorize and capture the funds at the time the payment is processed'),
      'authorization' => t('Authorization - reserve funds on the card to be captured later through your Realex account'),
    ),
    '#default_value' => $settings['payment_action'],
  );
  
  $form['ipn_logging'] = array(
    '#type' => 'radios',
    '#title' => t('IPN logging'),
    '#options' => array(
      'notification' => t('Log notifications during IPN validation and processing.'),
      'full_ipn' => t('Log notifications with the full IPN during validation and processing (used for debugging).'),
    ),
    '#default_value' => $settings['ipn_logging'],
  );

  // Invoke the hook here so other modules can alter the form
  foreach (module_implements('commerce_realex_redirect_settings_form') as $module) {
    $function = $module . '_commerce_realex_redirect_settings_form';
    $function($form, $form_state, $settings);
  }
  
  return $form;
}

/**
 * Payment method callback: adds a message and CSS to the submission form.
 */
function commerce_realex_redirect_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  $form['realex_wps_information'] = array(
    '#markup' => '<span class="commerce-realex-wps-info">' . t('(Continue with checkout to complete payment via Realex.)') . '</span>',
    '#attached' => array(
      'css' => array(drupal_get_path('module', 'commerce_realex_redirect') . '/theme/commerce_realex_redirect.css'),
    ),
  );

  return $form;
}

/**
 * Payment method callback: redirect form, a wrapper around the module's general
 *   use function for building a realex form.
 */
function commerce_realex_redirect_redirect_form($form, &$form_state, $order, $payment_method) {
  // Return an error if the enabling action's settings haven't been configured.
  if (empty($payment_method['settings']['merchant_id'])) {
    drupal_set_message(t('Realex is not configured for use. No Realex merchant ID has been specified.'), 'error');
    return array();
  }

  $settings = array(
    // Return to the previous page when payment is canceled
    'cancel_return' => url('checkout/' . $order->order_id . '/payment/back/' . $order->data['payment_redirect_key'], array('absolute' => TRUE)),

    // Return to the payment redirect page for processing successful payments
    'return' => url('checkout/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key'], array('absolute' => TRUE)),

    // Specify the current payment method instance ID in the notify_url
    'payment_method' => $payment_method['instance_id'],
  );

  return commerce_realex_redirect_order_form($form, $form_state, $order, $payment_method['settings'] + $settings);
}

/**
 * Payment method callback: redirect form return validation.
 */
function commerce_realex_redirect_redirect_form_validate($order, $payment_method) {
  if (!empty($payment_method['settings']['ipn_logging']) &&
    $payment_method['settings']['ipn_logging'] == 'full_ipn') {
    watchdog('commerce_realex_redirect', 'Customer returned from Realex with the following POST data:<pre>' . check_plain(print_r($_POST, TRUE)) . '</pre>', array(), WATCHDOG_NOTICE);
  }

  // This may be an unnecessary step, but if for some reason the user does end
  // up returning at the success URL with a Failed payment, go back.
  if (!empty($_POST['payment_status']) && $_POST['payment_status'] == 'Failed') {
    return FALSE;
  }
}

/**
 * Payment method callback: validate an IPN based on receiver e-mail address,
 *   price, and other parameters as possible.
 */
function commerce_realex_redirect_realex_ipn_validate($order, $payment_method, $ipn) {
  // Return FALSE if the receiver e-mail does not match the one specified by
  // the payment method instance.
  /*
  if ($ipn['member_id'] != $payment_method['settings']['merchant_id']) {
    commerce_payment_redirect_pane_previous_page($order);
    watchdog('commerce_realex_redirect', 'IPN rejected: invalid receiver e-mail specified (@member_id).', array('@member_id' => $ipn['member_id']), WATCHDOG_NOTICE);
    return FALSE;
  }
 */
  watchdog('commerce_realex_redirect', 'IPN validated for Order @order_number with ID @pmt_ref.', array('@order_number' => $order->order_number, '@pmt_ref' => $ipn['pmt_ref']), WATCHDOG_NOTICE);
}

/**
 * Payment method callback: process an IPN once it's been validated.
 */
function commerce_realex_redirect_realex_ipn_process($order, $payment_method, &$ipn) {
  // Exit when we don't get a result we don't recognise
  //watchdog('commerce_realex_redirect', t('Payment !ref, processing: !message', array('!ref' => $ipn['saved_pmt_ref'], '!message' => $ipn['pmt_setup_msg'])));
  /*
  if ($ipn['result'] != 00) {
    //watchdog('commerce_realex_redirect', t('Payment !ref unsuccessful: !message', array('!ref' => $ipn['saved_pmt_ref'], '!message' => $ipn['pmt_setup_msg'])));
	drupal_set_message(t('Payment unsuccessful: !message', array('!message' => $ipn['pmt_setup_msg'])));
    commerce_payment_redirect_pane_previous_page($order);
    return FALSE;
  }
  */
  // If this is a prior authorization capture IPN for which we've already
  // created a transaction...
  /*
  if (in_array($ipn['payment_status'], array('Voided', 'Completed')) &&
    !empty($ipn['auth_id']) && $auth_ipn = commerce_realex_redirect_ipn_load($ipn['auth_id'])) {
    // Load the prior IPN's transaction and update that with the capture values.
    $transaction = commerce_payment_transaction_load($auth_ipn['transaction_id']);
  }
  else {
  */
    // Create a new payment transaction for the order.
    $transaction = commerce_payment_transaction_new('realex', $order->order_id);
    $transaction->instance_id = $ipn['saved_pmt_ref'];
  //}

  $transaction->remote_id = $ipn['saved_pmt_ref'];
  $transaction->amount = commerce_currency_decimal_to_amount($ipn['amount'], $ipn['currency']);
  $transaction->currency_code = $ipn['currency'];
  $transaction->payload[REQUEST_TIME] = $ipn;

  // Set the transaction's statuses based on the IPN's payment_status.
  $transaction->remote_status = $ipn['pmt_setup'];

  // If we didn't get an approval response code...
  switch ($ipn['pmt_setup']) {
	case 200:
	case 205:
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      $transaction->message = t("The payment has failed: !message.", array('!message' => $ipn['pmt_setup_msg']));
      break;

	// Not sure yet what this response means...
    case 101:
	case 102:
      $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;
      $transaction->message = t("The payment has been referred: !message.", array('!message' => $ipn['pmt_setup_msg']));
      break;

    case 00:
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      $transaction->message = t("The payment has been successful.");
      break;
    //  TODO
	/*
    case 'Refunded':
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      $transaction->message = t('Refund for transaction @pmt_ref', array('@pmt_ref' => $ipn['saved_pmt_ref']));
      break;
	  */
  }

  // Save the transaction information.
  commerce_payment_transaction_save($transaction);
  $ipn['transaction_id'] = $transaction->transaction_id;
  commerce_payment_redirect_pane_next_page($order);
  watchdog('commerce_realex_redirect', 'IPN processed for Order @order_number with ID @pmt_ref.', array('@pmt_ref' => $ipn['saved_pmt_ref'], '@order_number' => $order->order_number), WATCHDOG_INFO);
}

/**
 * Builds a Realex Payments form from an order object.
 *
 * @param $order
 *   The fully loaded order being paid for.
 * @param $settings
 *   An array of settings used to build out the form, including:
 *   - merchant_id: the Realex e-mail address the payment submits to
 *   - cancel_return: the URL Realex should send the user to on cancellation
 *   - return: the URL Realex should send the user to on successful payment
 *   - currency_code: the Realex currency code to use for this payment if the
 *     total for the order is in a non-Realex supported currency
 *   - language: the Realex language code to use on the payment form
 *   - payment_action: the Realex payment action to use: sale, authorization,
 *     or order
 *   - payment_method: optionally the name of the Commerce payment method to
 *     include in the IPN notify_url
 *
 * @return
 *   A renderable form array.
 */
function commerce_realex_redirect_order_form($form, &$form_state, $order, $settings) {
  $wrapper = entity_metadata_wrapper('commerce_order', $order);

  $currency_code = $wrapper->commerce_order_total->currency_code->value();
  $amount = $wrapper->commerce_order_total->amount->value();
  $account = 'bodytab';
  
  // Ensure a default value for the payment_method setting.
  $settings += array('payment_method' => '');
  //watchdog('commerce_sp', 'settings '.print_r($settings,1));
  //  Obtain values for authentication
  $timestamp = strftime("%Y%m%d%H%M%S");
  mt_srand((double)microtime()*1000000);
  $orderid = $order->order_id;
  $merchantid = $settings['merchant_id'];
  $secret = $settings['shared_secret'];

  $payer_ref = $order->uid;
  $pmt_ref = $order->order_id .'-'. $order->uid;
  
  
  
  //  TIMESTAMP.MERCHANT_ID.ORDER_ID.AMOUNT.CURRENCY
  $tmp = "$timestamp.$merchantid.$orderid.$amount.$currency_code";
  $sha1hash = sha1($tmp);
  $tmp = "$sha1hash.$secret";
  $sha1hash = sha1($tmp);

  // Build the data array that will be translated into hidden form values.
  $data = array(
  
    // The store's merchant id
    'MERCHANT_ID' => $merchantid,

    // The order number
    'ORDER_ID' => $orderid,    
    
    'AMOUNT' => $amount,

    //'ACCOUNT' => $account,

    // Set the currency and language codes
    'CURRENCY' => $currency_code,

    // The time of payment
    'TIMESTAMP' => $timestamp,

    // The reference for the payer
    'PAYER_REF' => $payer_ref,

    // A reference number to give the payment
    'PMT_REF' => $pmt_ref,

    // This has to be set to 1.  It tells the server to do a payer exist check
    'PAYER_EXIST' => 1,

    // The hash
    'SHA1HASH' => $sha1hash,

    // Settle automatically
    'AUTO_SETTLE_FLAG' => 1,

    // Specify the checkout experience to present to the user.
    //'CMD' => '_cart',

    // Signify we're passing in a shopping cart from our system.
    //'UPLOAD' => 1,

    // The path Realex should send the IPN to
    //'NOTIFY_URL' => 'commerce_realex/ipn',

    // Set the correct character set
    //'CHARSET' => 'utf-8',

    // Do not display a comments prompt at Realex
    //'NO_NOTE' => 1,

    // Do not display a shipping address prompt at Realex
    //'NO_SHIPPING' => 1,

    // Return to the review page when payment is canceled
    //'CANCEL_RETURN' => $settings['cancel_return'],

    // Return to the payment redirect page for processing successful payments
    //'RETURN' => $settings['return'],

    // Return to this site with payment data in the POST
    //'RM' => 2,

    //'LC' => $settings['language'],

    // Define a single item in the cart representing the whole order
    'ITEM_NAME' => t('Order @order_number at @store', array('@order_number' => $order->order_number, '@store' => variable_get('site_name', url('<front>', array('absolute' => TRUE))))),
    //'on0_1' => t('Product count'),
    //'os0_1' => commerce_line_items_quantity($wrapper->commerce_line_items, commerce_product_line_item_types()),
  );
  
  // Invoke the hook here so implementations have access to the form
  foreach (module_implements('commerce_realex_redirect_order_form') as $module) {
    $function = $module . '_commerce_realex_redirect_order_form';
	watchdog('commerce_realex_redirect', 'hook '.print_r($module,1));
    $function($data, $order, $settings);
  }
  watchdog('commerce_realex_redirect', 'request '.print_r($data,1));
  $form['#action'] = $settings['server'];
  
  foreach ($data as $name => $value) {
    if (!empty($value)) {
      $form[$name] = array('#type' => 'hidden', '#value' => $value);
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Proceed to Realex'),
  );

  return $form;
}


/**
 * Returns an array of all possible language codes.
 */
function commerce_realex_redirect_languages() {
  return drupal_map_assoc(array('AU', 'DE', 'FR', 'IT', 'GB', 'ES', 'US'));
}

/**
 * Returns an array of all possible currency codes.
 */
function commerce_realex_redirect_currencies() {
  return drupal_map_assoc(array('AUD', 'BRL', 'CAD', 'CHF', 'CZK', 'DKK', 'EUR', 'GBP', 'HKD', 'HUF', 'ILS', 'JPY', 'MXN', 'MYR', 'NOK', 'NZD', 'PHP', 'PLN', 'SEK', 'SGD', 'THB', 'TWD', 'USD'));
}
