Realex Payment Recurring
----------------------
A Payment method for recurring payments using the realex redirect payment gateway.  Use this in conjunction with the commerce_recurring module at http://drupal.org/project/commerce_recurring which manages the recurring payments using the cardonfile module.

Installation
------------
 - Download and enable the module
 - Go to the payment methods settings page at
 admin/commerce/config/payment-methods
 - Click the edit on the "Realex Redirect" Rule and then again on the action
 for the rule.
 - Ensure that the receipt-in url matches that provided by the vendor.  This is different from the url used for a Redirect Payment
