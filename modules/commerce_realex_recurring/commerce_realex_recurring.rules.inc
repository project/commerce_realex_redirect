<?php

/**
 * Rules integration for recurring triggers.
 */


/**
 * Implements hook_rules_action_info().
 */
function commerce_realex_recurring_rules_action_infoxx() {
  $actions = array();
  
  $actions['commerce_realex_recurring_load_recurring'] = array(
    'label' => t('Load recurring orders'),
    'parameter' => array(),
    'provides' => array(
      'orders' => array(
        'type' => 'list<commerce_order>',
        'label' => t('Recurring Orders'),
        'description' => t('Recurring orders'),
      ),
    ),
    'group' => t('Commerce Realex Recurring'),
  );

  $actions['commerce_realex_recurring_load_expired_users'] = array(
    'label' => t('Load recurring user that have expired'),
    'parameter' => array(),
    'provides' => array(
      'users' => array(
        'type' => 'list<user>',
        'label' => t('Expired users'),
        'description' => t('Users whose accounts have expired'),
      ),
    ),
    'group' => t('Commerce Realex Recurring'),
  );

  $actions['commerce_realex_recurring_load_payments_due'] = array(
    'label' => t('Load recurring orders with due payments'),
    'parameter' => array(),
    'provides' => array(
      'orders' => array(
        'type' => 'list<commerce_order>',
        'label' => t('Recurring Orders'),
        'description' => t('Due recurring orders'),
      ),
    ),
    'group' => t('Commerce Realex Recurring'),
  );
  

  return $actions;
}


/**
 * Rules action callback: Load orders
*/
function commerce_realex_recurring_load_recurring() {
  watchdog('commerce_recurring', 'start of func ');
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'commerce_order');
  $query->entityCondition('bundle', 'commerce_order');
  $now = new DateObject('now');
  $query->fieldCondition('commerce_realex_recurring_next_due', 'value', $now->format('U'), '<=');
  $query->range(0, variable_get('commerce_realex_recurring_batch_process', 20));
  $results = $query->execute();
  
  watchdog('commerce_recurring', 'results '.print_r($results,1));
  if (!empty($results['commerce_order'])) {
    
    return array('orders' => commerce_order_load_multiple(array_keys($results['commerce_order'])));
  }
  return array('orders' => array());
}

/**
 * Rules action callback: Load orders
*/
function commerce_realex_recurring_load_payments_due() {
  $query = db_select('commerce_order', 'co', array('fetch' => PDO::FETCH_ASSOC));
  $query->join('field_data_commerce_recurring_next_due', 'nd', 'nd.entity_id = co.order_id');
  $query->fields('co', array('order_id', 'type', 'revision_id'));
  $query->condition('nd.commerce_recurring_next_due_value', time(), '<=');
  $orders = $query->execute();
  $results = array();
  foreach ($orders as $result) {
    $results['commerce_order'][$result['order_id']] = $result;
  }
  if (!empty($results['commerce_order'])) {
    return array('orders' => commerce_order_load_multiple(array_keys($results['commerce_order'])));
  }
  return array('orders' => array());
}

/**
 * Rules action callback: Load users
 */
function commerce_realex_recurring_load_expired_users() {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'user');
  $query->entityCondition('bundle', 'user');
  $now = new DateObject('now');
  $query->fieldCondition('commerce_realex_recurring_expires', 'value', $now->format('U'), '<=');
  $query->fieldCondition('commerce_realex_recurring_auto_renew', 'value', 'off', '=');
  $query->range(0, variable_get('commerce_realex_recurring_batch_process', 20));
  $results = $query->execute();
  if (!empty($results['user'])) {
    return array('users' => user_load_multiple(array_keys($results['user'])));
  }
  return array('users' => array());
}