<?php watchdog('commerce_realex', 'variables <pre>'.print_r($variables,1) .'</pre>');?>
<request type="receipt-in" timestamp="<?php print $variables['timestamp']?>">
<merchantid><?php print $variables['merchant_id']?></merchantid>
<account>internet</account>
<?php // The order id of the cloned order should be used here  ?>
<orderid><?php print $variables['order_id']?></orderid>
<amount currency="<?php print $variables['currency']?>"><?php print $variables['amount']?></amount>
<payerref><?php print $variables['payer_ref']?></payerref>
<paymentmethod><?php print $variables['pmt_type']?></paymentmethod>
<?php // When this flag is set to 1 then payment is immediate  ?>
<autosettle flag="1" />
<md5hash />
<sha1hash><?php print $variables['sha1hash']?></sha1hash>
<comments>
<comment id="1" />
<comment id="2" />
</comments>
<recurring type="fixed" sequence="<?php print $variables['sequence']?>"></recurring>
<tssinfo>
<address type="billing">
<code />
<country />
</address>
<address type="shipping">
<code />
<country />
</address>
<custnum></custnum>
<varref></varref>
<prodid><?php print $variables['prod_id']?></prodid>
</tssinfo>
<supplementarydata>
<item type="pnr">
<field01>238002</field01>
</item>
</supplementarydata>
</request>